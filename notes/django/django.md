Django
======

## Quality assurance

### Pylint
> Pylint is a source-code, bug and quality checker for the Python programming
language. It is named following a common convention in Python of a "py" prefix,
and a nod to the C programming lint program. It follows the style recommended by
PEP 8, the Python style guide.

#### Install
1. Install dependencies
    ```bash
    pip install pylint-django
    ```

0. Set `DJANGO_SETTINGS_MODULE`
    ```bash
    DJANGO_SETTINGS_MODULE=your.app.settings pylint --load-plugins pylint_django [..other options..] <path_to_your_sources>
    ```
    for example:
    ```bash
    DJANGO_SETTINGS_MODULE=poppin.settings pylint --load-plugins pylint_django ./**/*.py
    ```

0. Copy [.pylintrc](/example_settings_files/.pylintrc)

0. Run pylint
  ```bash
  pylint --load-plugins=pylint_django --rcfile .pylintrc ./**/*.py
  ```

### Django-nose
> Coverage of tests

### Install
1. Install dependencies
    ```bash
    pip install django-nose coverage
    ```
0. Modify **project's settings** file:
    1. Declare app on `INSTALLED_APPS`
        ```bash
        INSTALLED_APPS = (
          ...
          'django_nose',
          ...
        )
        ```

    0. Set _django_nose_ as test runner
      ```bash
      TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'
      ```
0. Copy [.coveragerc]()


## Tips and tricks
### Iterate through model fields:
```python
obj = patients_models.Paciente.objects.get(pk=1)

for field in patients_models.Paciente._meta.get_fields():
    print(field.name, getattr(obj,field.name, None))
```
**Return None when its a reference field. Is empty `""` when the field value
is empty**

### Dumpdata
```console
python manage.py dumpdata --natural-foreign --natural-primary <app>.<model> -e contenttypes -o users/fixtures/users/users.json
```

