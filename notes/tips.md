Tips
====


## Lists
### Iterate 2 lists at the same time
```python
for f, b in zip(foo, bar):
  print(f, b)
```
`zip` stops when the shorter of `foo` or `bar` stops

## Dicts
### Pass dict as kwargs
> https://stackoverflow.com/questions/5710391/converting-python-dict-to-kwargs
```python
dict = {
  "foo": "juan",
  "hoo": 1
}

# **dict -> foo="juan", hoo=1
```
